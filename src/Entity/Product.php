<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=355)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantity;


    /**
     * @Assert\NotBlank(message="Please, upload the product image as .jpg, .jpeg or .png file.")
     * @Assert\File(mimeTypes={ "image/jpeg", "image/png" })
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=650)
     */
    private $imageName;



    public function getId()
    {
        return $this->id;
    }

    public function getName() : ? string
    {
        return $this->name;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice() : ? int
    {
        return $this->price;
    }

    public function setPrice(int $price) : self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription() : ? string
    {
        return $this->description;
    }

    public function setDescription(string $description) : self
    {
        $this->description = $description;

        return $this;
    }

    public function getQuantity() : ? int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity) : self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getImage() : ? UploadedFile
    {
        return $this->image;
    }

    public function setImage(UploadedFile $image) : self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageName() : ? string
    {
        return $this->imageName;
    }

    public function setImageName(string $imageName) : self
    {
        $this->imageName = $imageName;

        return $this;
    }
}
