<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\ProductRepository;
use App\Service\FileUploader;

class GestionAddController extends Controller
{
    /**
     * @Route("/gestion/add/admin", name="gestion/add")
     * @Route("/gestion/{id}/modif/admin", name="gestion_modif")
     */
    public function addModifProduct(Request $request, Product $product = null, ObjectManager $manager,FileUploader $fileUploader)
    {
        if (!$product) {
            $product = new Product();
        }

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $product->getImage();
            $fileName = $fileUploader->upload($file);

            $product->setImageName($fileName);

            $manager->persist($product);
            $manager->flush();

            return $this->redirectToRoute('full_page_product');
        }

        return $this->render('gestion_add/index.html.twig', [
            'controller_name' => 'GestionAddController',
            'form' => $form->createView(),
            'edit' => $product->getId() !== null

        ]);

    }

    

}
